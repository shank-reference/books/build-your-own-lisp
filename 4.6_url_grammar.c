#include "mpc.h"

int main(int argc, char** argv) {
  // http://www.buildyourownlisp.com

  mpc_parser_t* Protocol = mpc_new("protocol");
  mpc_parser_t* SubDomain = mpc_new("sub_domain");
  mpc_parser_t* Domain = mpc_new("domain");
  mpc_parser_t* Tld = mpc_new("tld");
  mpc_parser_t* URL = mpc_new("url");

  mpca_lang(MPCA_LANG_DEFAULT,
            "protocol: \"http\" | \"https\";"
            "sub_domain: \"www\" | \"\";"
            "domain: \"%s\""
            "tld: \"com\";"
            "url: <protocol>.//<sub_domain>.<domain>.<tld>",
            Protocol, SubDomain, Domain, Tld, URL);

  mpc_cleanup(5, Protocol, SubDomain, Domain, Tld, URL);

  return 0;
}