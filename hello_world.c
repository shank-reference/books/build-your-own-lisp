#include <stdio.h>

typedef struct {
  float x;
  float y;
} point;

int add_together(int x, int y) { return x + y; }

void check_point() {
  point p;
  p.x = 10.0;
  p.y = 1;
  float length = sqrt(p.x * p.x + p.y * p.y);
}

int main(int argc, char **argv) {
  puts("Hello World!");

  int c = add_together(1, 2);

  check_point();

  return 0;
}