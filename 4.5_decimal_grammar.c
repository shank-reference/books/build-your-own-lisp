#include "mpc.h"

int main(int argc, char** argv) {
  mpc_parser_t* Digit = mpc_new("digit");
  mpc_parser_t* Dot = mpc_new("dot");
  mpc_parser_t* Decimal = mpc_new("decimal");

  mpca_lang(MPCA_LANG_DEFAULT,
            "digit: \"0\" | \"1\" | \"2\" | \"3\" | \"4\" | \"5\" | \"6\" | "
            "\"7\" | \"8\" | \"9\";"
            "dot:  \".\";"
            "decimal: <digit>+ <dot> <digit>* ",
            Digit, Dot);

  mpc_cleanup(3, Digit, Dot, Decimal);

  return 0;
}